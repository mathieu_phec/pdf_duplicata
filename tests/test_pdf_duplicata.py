import pytest

import pdf_duplicata
from pdf_duplicata.main import duplicata, duplicatas

def test_one_duplicata():
    """
        Test le duplicata d'un fichier PDF d'une page
    """
    pdf_dup = duplicata(
        source_pdf = "tests/data/single/Facture_FR_BASIC.pdf",
        source_duplicata = "tests/data/tampon_duplicata.pdf"
    )

    # Le resultat ne doit pas être None
    assert pdf_dup is not None

    # Le resultat doit etre ./tests/data/single/Facture_FR_BASIC_DUPLICATA.pdf
    assert pdf_dup == "tests/data/single/Facture_FR_BASIC_DUPLICATA.pdf"

def test_duplicata_no_args():
    """
        Test d'un duplicata sans argument
    """
    # Sans aucun arguments
    with pytest.raises(Exception):
        duplicata()

    # Avec un seul argument
    with pytest.raises(Exception, match="La source Duplicata est obligatoire !"):
        pdf_dup = duplicata(
            source_pdf = "tests/data/single/Facture_FR_BASIC.pdf"
        )

    with pytest.raises(Exception, match="La source PDF est obligatoire !"):
        pdf_dup = duplicata(
            source_duplicata = "tests/data/tampon_duplicata.pdf"
        )

def test_many_duplicata():
    """
        Test le duplicata avec en source un répertoire
    """
    pdf_dup = duplicatas(
        source_pdfs = "tests/data/many",
        source_duplicata = "tests/data/tampon_duplicata.pdf"
    )

    # Le resultat ne doit pas être None
    assert pdf_dup is not None

    # Le resultat doit etre ./tests/data/Facture_FR_BASIC_DUPLICATA.pdf
    assert pdf_dup == "tests/data/many"

def test_duplicatas_no_args():
    """
        Test d'un duplicata sans argument
    """
    # Sans aucun arguments
    with pytest.raises(Exception):
        duplicatas()

    # Avec un seul argument
    with pytest.raises(Exception, match="La source Duplicata est obligatoire !"):
        pdf_dup = duplicatas(
            source_pdfs = "tests/data/many"
        )

    with pytest.raises(Exception, match="Le chemin contenant les PDFs est obligatoire !"):
        pdf_dup = duplicatas(
            source_duplicata = "tests/data/tampon_duplicata.pdf"
        )
import sys
import re

def change_version_file(file_path, regex_in, version_string):
    """
        Change la version dans le fichier file_path
        Recherche la chaine REGEX regex_in et remplace par version_string
        Sauvegarde le fichier 
    """
    # On ouvre le fichier
    with open(file_path, "r+") as f:
        # On charge le contenu
        content = f.read()
        f.seek(0)
        # On remplate les valeurs via un REGEX sub
        res = re.sub(regex_in, version_string, content)
        # On écrit
        f.write(res)
        f.truncate()


assert len(sys.argv) == 2, "version arg is missing !"

version = sys.argv[1].strip()
print("set-version to " + version)

# Change le fichier pyproject.toml
change_version_file("pyproject.toml", r"version = ['\"]([^'\"]*)['\"]", 'version = "%s"' %version)

# Change le ficier pdf_duplicata/__init__.py
change_version_file("pdf_duplicata/__init__.py", r"__version__ = ['\"]([^'\"]*)['\"]", '__version__ = "%s"' %version)
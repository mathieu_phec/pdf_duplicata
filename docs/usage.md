# Utilisation

PDF Duplicata est accessible via un CLI ainsi qu'en module python

## CLI

Pour utiliser PDF Duplicata via le CLI :

```bash
pdf_duplicata SOURCE_PDF SOURCE_DUPLICATA
```

Par exemple :
```
pdf_duplicata tests/data/many/Facture_FR_BASIC.pdf tests/data/tampon_duplicata.pdf
✔️  Duplicata du PDF dans tests/data/many/Facture_FR_BASIC_DUPLICATA.pdf
```

Vous pouvez également utiliser l'option --use-path vous permettant ainsi de traiter le répertoire et les répertoires enfants.

Par exemple :
```
pdf_duplicata --use-path tests/data/many tests/data/tampon_duplicata.pdf
✔️  Duplicata des PDFs dans tests/data/many
```

## Module

Pour utiliser PDF Duplicata en tant que module Python :

```python
from pdf_duplicata.main import duplicata
duplicata(SOURCE_PDF, SOURCE_DUPLICATA)
```
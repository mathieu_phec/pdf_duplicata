from pathlib import Path
from PyPDF2 import PdfFileReader, PdfFileWriter


def duplicata(source_pdf=None, source_duplicata=None):
    """
        Fonction permettant de faire un duplicata (merge)
        Retourne le chemin du PDF dupliqué avec pour nom NOM_PDF_DUPLICATA.pdf
    """
    # On vérifie les arguments
    if source_pdf is None:
        raise Exception("La source PDF est obligatoire !")
    if source_duplicata is None:
        raise Exception("La source Duplicata est obligatoire !")

    # On converti l'ensemble en objet path
    source_pdf_path = Path(source_pdf)
    source_duplicata_path = Path(source_duplicata)
    #  On genere le nom du fichier de sortie
    destination_name = "%s_DUPLICATA%s" % (source_pdf_path.stem, source_pdf_path.suffix)
    destination_pdf_path = source_pdf_path.parent / destination_name

    # On charge le pdf source
    spdf = PdfFileReader(source_pdf_path.open(mode="rb"))

    # On charge le template duplicata
    sdup = PdfFileReader(source_duplicata_path.open(mode="rb"))

    # On instancie notre objet de resultat
    sout = PdfFileWriter()

    # On boucle sur l'ensemble des pages du fichier source
    for i in range(spdf.getNumPages()):
        # On recupere la page courante
        page = spdf.getPage(i)
        # On merge avec la premiere page de notre duplicata
        page.mergePage(sdup.getPage(0))
        # On ajoute l'ensemble au résultat
        sout.addPage(page)

    #  On écrit le fichier de sortie
    with destination_pdf_path.open(mode="wb") as output_file:
        sout.write(output_file)

    # On renvoie le chemin du fichier
    return str(destination_pdf_path)


def duplicatas(source_pdfs=None, source_duplicata=None):
    """
        Fonction permettant de faire un duplicata (merge) de PDFs
        présent dans un répertoire donnée.
        Retourne le chemin contenant les PDFs dupliqué
        Utilise la fonction duplicata
    """
    # On vérifie les arguments
    if source_pdfs is None:
        raise Exception("Le chemin contenant les PDFs est obligatoire !")
    if source_duplicata is None:
        raise Exception("La source Duplicata est obligatoire !")

    # On converti l'ensemble en objet path
    source_pdfs_path = Path(source_pdfs)

    # On boucle sur les fichiers
    for source_pdf in source_pdfs_path.glob("**/*.pdf"):
        duplicata(source_pdf=source_pdf, source_duplicata=source_duplicata)

    # On renvoie le chemin contenant les PDFs dupliqués
    return str(source_pdfs_path)

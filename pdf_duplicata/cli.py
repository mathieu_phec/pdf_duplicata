import typer

from . import __version__
from .main import duplicata, duplicatas


def version_callback(value: bool):
    if value:
        typer.echo(f"PDF Duplicata Version: {__version__}")
        raise typer.Exit()


def run(
    source_pdf: str,
    source_duplicata: str,
    use_path: bool = False,
    version: bool = typer.Option(
        None, "--version", callback=version_callback, is_eager=True
    ),
):
    if use_path:
        result = duplicatas(source_pdf, source_duplicata)
        typer.echo("✔️  Duplicata des PDFs dans %s" % result)
    else:
        result = duplicata(source_pdf, source_duplicata)
        typer.echo("✔️  Duplicata du PDF dans %s" % result)


def main():
    typer.run(run)


if __name__ == "__main__":
    main()

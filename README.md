# PDF Duplicata

[![pipeline status](https://gitlab.com/mathieu_phec/pdf_duplicata/badges/master/pipeline.svg)](https://gitlab.com/mathieu_phec/pdf_duplicata/-/commits/master)
[![coverage report](https://gitlab.com/mathieu_phec/pdf_duplicata/badges/master/coverage.svg)](https://gitlab.com/mathieu_phec/pdf_duplicata/-/commits/master)

[![code_testing_python](https://gitlab.com/mathieu_phec/pdf_duplicata/-/jobs/artifacts/master/raw/code_testing_python.png?job=code_testing_python)](https://gitlab.com/mathieu_phec/pdf_duplicata/-/commits/master)
[![secret_detection](https://gitlab.com/mathieu_phec/pdf_duplicata/-/jobs/artifacts/master/raw/secret_detection.png?job=secret_detection)](https://gitlab.com/mathieu_phec/pdf_duplicata/-/commits/master)
[![dependency_vulnerability_python](https://gitlab.com/mathieu_phec/pdf_duplicata/-/jobs/artifacts/master/raw/dependency_vulnerability_python.png?job=dependency_vulnerability_python)](https://gitlab.com/mathieu_phec/pdf_duplicata/-/commits/master)
[![code_scanning_python](https://gitlab.com/mathieu_phec/pdf_duplicata/-/jobs/artifacts/master/raw/code_scanning_python.png?job=code_scanning_python)](https://gitlab.com/mathieu_phec/pdf_duplicata/-/commits/master)
[![code_linting_python](https://gitlab.com/mathieu_phec/pdf_duplicata/-/jobs/artifacts/master/raw/code_linting_python.png?job=code_linting_python)](https://gitlab.com/mathieu_phec/pdf_duplicata/-/commits/master)
[![code_formatting_python](https://gitlab.com/mathieu_phec/pdf_duplicata/-/jobs/artifacts/master/raw/code_formatting_python.png?job=code_formatting_python)](https://gitlab.com/mathieu_phec/pdf_duplicata/-/commits/master)
[![docs](https://gitlab.com/mathieu_phec/pdf_duplicata/-/jobs/artifacts/master/raw/documentation.png?job=pages)](https://gitlab.com/mathieu_phec/pdf_duplicata/-/commits/master)

Une librairie Python permettant d'effectuer des dupplicatas d'un PDF via l'ajout d'une page en filigramme sur l'ensemble des pages du document source.

Pour plus d'informations, une [documentation est présente](https://mathieu_phec.gitlab.io/pdf_duplicata/)


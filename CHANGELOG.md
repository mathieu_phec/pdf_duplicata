## [2.1.1](https://gitlab.com/mathieu_phec/pdf_duplicata/compare/v2.1.0...v2.1.1) (2020-07-16)


### Bug Fixes

* **python:** Add support only for Python >= 3.6 ([b02fd24](https://gitlab.com/mathieu_phec/pdf_duplicata/commit/b02fd24a0b7419b3b9ecab795672325bcd0bf835))

# [2.1.0](https://gitlab.com/mathieu_phec/pdf_duplicata/compare/v2.0.0...v2.1.0) (2020-07-16)


### Features

* Add support to Python >= 3.0 ([e5edc68](https://gitlab.com/mathieu_phec/pdf_duplicata/commit/e5edc68b5efa8e9c8965a1503fe3d6908797e620))

# [2.0.0](https://gitlab.com/mathieu_phec/pdf_duplicata/compare/v1.2.0...v2.0.0) (2020-07-15)


### Features

* Bump version ([b392910](https://gitlab.com/mathieu_phec/pdf_duplicata/commit/b392910e7b389991b3599ea9cd19b925907cd6b3))


### BREAKING CHANGES

* Bump to major release

# [1.2.0](https://gitlab.com/mathieu_phec/pdf_duplicata/compare/v1.1.1...v1.2.0) (2020-07-15)


### Features

* BREAKING CHANGE Add duplicata for many PDFs ([ab0097c](https://gitlab.com/mathieu_phec/pdf_duplicata/commit/ab0097c54cdbd4f15fe4b4b83d393f5e32075347)), closes [#5](https://gitlab.com/mathieu_phec/pdf_duplicata/issues/5)
